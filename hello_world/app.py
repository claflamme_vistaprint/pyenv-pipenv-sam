import json


def lambda_handler(event, context):
    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "Oh! What's up Steve?!",
        }),
    }

resp = lambda_handler(1, 1)

print(resp)