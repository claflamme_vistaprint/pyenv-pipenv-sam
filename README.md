# pyenv_pipenv

1. Make sure you have `pipenv` installed:

    ```bash
    brew install pipenv # mac
    ```

2. Create a new virtual environment:

    ```bash
    pipenv shell
    ```

    1. Notice that a `Pipfile` has been created in the root of your directory.
    2. Notice that instead of having a `venv` directory created at the root here it puts it in a central location by default:

        ```bash
        ✔ Successfully created virtual environment! 
        Virtualenv location: /Users/claflamme/.local/share/virtualenvs/pyenv_pipenv-8jeHr6HM
        Creating a Pipfile for this project…
        Launching subshell in virtual environment…
        . /Users/claflamme/.local/share/virtualenvs/pyenv_pipenv-8jeHr6HM/bin/activate
        ```

    3. Notice that this new virtual environment is automatically activated for you and you don't need to remember to run `venv/bin/source/activate`:

        ```bash
        (pyenv_pipenv)
        ~/Source/SystemsEngineering/Examples/pyenv_pipenv » which python
        claflamme@WLT041285/Users/claflamme/.local/share/virtualenvs/pyenv_pipenv-8jeHr6HM/bin/python

3. Install your packages:

   ```bash
   # boto3, for example
   pipenv install boto3
   ```

4. Install development specific packages:

    ```bash
    # pytest, for example
    pipenv install pytest --dev
    ```

## Deploying a Lambda Function Using the AWS Serverless Application Model (SAM)

1. Run a build command to stage your project for deployment:

    ```bash
    sam build
    ```

2. Package things up (not sure what's happening here right now):

    ```bash
    run sam package --s3-bucket 011569915708-sam-deploy-artifacts \
    --output-template-file template_packaged.yaml
    ```

3. Deploy the thing already!:

    ```bash
    sam deploy --template-file template_packaged.yaml \
    --stack-name DEV-Hello-World \
    --capabilities CAPABILITY_IAM \
    --tags "app=Hello-World" "env=DEV" "team=SysEng"
    ```
